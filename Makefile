# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vjarysta <vjarysta@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/11/20 14:08:14 by vjarysta          #+#    #+#              #
#    Updated: 2013/11/26 11:38:49 by vjarysta         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libft

SRCC = ft_memset.c ft_strlen.c ft_toupper.c ft_bzero.c ft_putchar.c ft_putstr.c ft_tolower.c ft_strcpy.c ft_strclr.c ft_memcpy.c ft_isalpha.c ft_isdigit.c ft_isalnum.c ft_isascii.c ft_isprint.c ft_strdup.c ft_memccpy.c ft_memchr.c ft_memcmp.c ft_strcat.c ft_strncat.c ft_strlcat.c ft_strstr.c ft_strcmp.c ft_strncmp.c ft_memmove.c ft_strncpy.c ft_atoi.c ft_strchr.c ft_strnstr.c ft_strrchr.c

SRCO = ft_memset.o ft_strlen.o ft_toupper.o ft_bzero.o ft_putchar.o ft_putstr.o ft_tolower.o ft_strcpy.o ft_strclr.o ft_memcpy.o ft_isalpha.o ft_isdigit.o ft_isalnum.o ft_isascii.o ft_isprint.o ft_strdup.o ft_memccpy.o ft_memchr.o ft_memcmp.o ft_strcat.o ft_strncat.o ft_strlcat.o ft_strstr.o ft_strcmp.o ft_strncmp.o ft_memmove.o ft_strncpy.o ft_atoi.o ft_strchr.o ft_strnstr.o ft_strrchr.o


all: $(NAME)

$(NAME):
	gcc -c -Wall -Werror -Wextra $(SRCC)
	ar rc $(NAME).a $(SRCO)

clean:
	/bin/rm -f $(SRCO)

fclean: clean
	/bin/rm -f $(NAME).a

re: fclean all
