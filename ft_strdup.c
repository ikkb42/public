/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjarysta <vjarysta@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 10:03:45 by vjarysta          #+#    #+#             */
/*   Updated: 2013/11/26 15:02:37 by vjarysta         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char	*ft_strdup(const char *s1)
{
	char	*dup;

	if  (dup)
	{	
		dup = (char *)malloc((ft_strlen(s1) + 1) * sizeof(char));
		dup[ft_strlen(s1)] = '\0';
		dup = ft_strcpy(dup, s1);
	}
	return (dup);
}
