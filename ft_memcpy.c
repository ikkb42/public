/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjarysta <vjarysta@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/21 16:09:52 by vjarysta          #+#    #+#             */
/*   Updated: 2013/11/26 10:48:12 by vjarysta         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *s1, const void *s2, size_t n)
{
	ft_putstr("memcpy");
	size_t		i;
	char		*temp1;
	char const	*temp2;

	i = 0;
	temp1 = (char *)s1;
	temp2 = (char const *)s2;

	while (i < n)
	{
		temp1[i] = temp2[i];
		i++;
	}
	return (s1);
}
