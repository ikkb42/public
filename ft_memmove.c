/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjarysta <vjarysta@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/24 17:45:33 by vjarysta          #+#    #+#             */
/*   Updated: 2013/11/27 10:49:23 by vjarysta         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *s1, const void *s2, size_t n)
{
	ft_putstr("memmove");
	char ps1[n];

	ft_memcpy(ps1, s2, n);
	ft_memcpy(s1, ps1, n);
	return (s1);
}
