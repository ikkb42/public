/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjarysta <vjarysta@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/21 14:53:38 by vjarysta          #+#    #+#             */
/*   Updated: 2013/11/26 14:49:17 by vjarysta         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_atoi(const char *str)
{
	int		sign;
	int		total;
	int		i;

	total = 0;
	sign = 1;
	i = 0;
	if (str)
	{
		if (str[i] == '-')
		{
			sign = -1;
			i++;
		}
		while (str[i] != '\0')
		{
			if (str[i] > '9' || str[i] < '0')
				return 0;
			total = total * 10;
			total = total + str[i] - '0';
			i++;
		}
	}
	return (total * sign);
}
