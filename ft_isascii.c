/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isascii.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjarysta <vjarysta@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 09:04:30 by vjarysta          #+#    #+#             */
/*   Updated: 2013/11/26 10:43:11 by vjarysta         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h" /* DELETE */

int		ft_isascii(int c)
{
	ft_putstr("isascii");
	if (c >= 0 && c <= 127)
		return (1);
	else
		return (0);
}
