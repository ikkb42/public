/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjarysta <vjarysta@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/21 16:09:52 by vjarysta          #+#    #+#             */
/*   Updated: 2013/11/27 09:30:18 by vjarysta         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *s1, const void *s2, int c, size_t n)
{
	ft_putstr("memccpy");
	size_t		i;
	char		*temp1;
	char const	*temp2;

	i = 0;
	temp1 = (char *)s1;
	temp2 = (const char *)s2;

	while (i < n)
	{
		temp1[i] = temp2[i];
		if (temp2[i] == c)
		{
			return ((char *)(temp1 + i + 1));
		}
		i++;
	}
	return (NULL);
}
