/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjarysta <vjarysta@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/25 11:45:59 by vjarysta          #+#    #+#             */
/*   Updated: 2013/11/26 10:54:57 by vjarysta         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strncat(char *s1, const char *s2, size_t n)
{
	ft_putstr("strncat");
	size_t	i;
	size_t	len;

	len = ft_strlen((const char*)s1);
	i = 0;

	while (i < n && s2[i] != '\0')
	{
		s1[len + i] = s2[i];
		i++;
	}
	s1[len + i] = '\0';

	return (s1);
}
